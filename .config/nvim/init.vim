"Plugins
call plug#begin('~/.vim/plugged')
Plug 'Yggdroot/indentLine'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/fzf.vim'
Plug 'lifepillar/vim-mucomplete'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
call plug#end()

"Behavior settings
set ai 										"auto-indent
set complete+=kspell 						"Match directory words	
set completeopt=menuone,longest 			"Auto-complete settings
set cursorline 								"Cursor line in insert mode
set incsearch 								"Auto-find search results
set noshowmode 								"Disables mode status beneath lightline
set noswapfile 								"Disables annoying swap notifications
set number 									"Line numbers
set shiftwidth=4 							"Spaces used for auto-indenting
set smartindent 							"Smart indenting
set splitbelow splitright 					"Split orientiation
set tabstop=4 								"Tab is 4 spaces
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"Color
syntax on
set background=dark
colorscheme base16-onedark
set termguicolors
highlight LineNr none
highlight Normal guibg=none
highlight NonText guibg=none
highlight StatusLine guifg=15

"KEYBINDS
map <C-l> :set cursorline!<CR>
map <C-p> "+P
map <C-x> "+d
nmap <leader>gc :Gcommit<CR>
nmap <leader>gh :diffget //3<CR>
nmap <leader>gs :G<CR>
nmap <leader>gu :diffget //2<CR>
nmap <leader>md <plug>MarkdownPreviewToggle
nnoremap <CR> :noh<CR><CR>
nnoremap <leader>b :Buffers<CR>
nnoremap <leader>c :wincmd c<CR>
nnoremap <leader>f :Files<CR>
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>r :w! \| !compiler <c-r>%<CR>
nnoremap <leader>sh :wincmd s<CR>
nnoremap <leader>sv :wincmd v<CR>
noremap <leader>n :call ToggleNetrw()<CR>
vnoremap <C-c> "+y

" Post/pre-write actions and formatting
autocmd BufWritePost ~/.Xresources !xrdb %
autocmd BufWritePost ~/Documents/Notes/Journal !gpg -c --batch --yes ~/Documents/Notes/Journal && rm ~/Documents/Notes/Journal
autocmd BufWritePre ~/.bash_aliases :sort
autocmd Bufread,BufNewFile *.txt set linebreak spell

"Latex
autocmd FileType tex set linebreak spell
autocmd FileType tex map <leader>z :!zathura $(echo % \| sed 's/tex$/pdf/') & disown<CR><CR>
autocmd FileType tex inoremap ,em \emph{}<Esc>T{i
autocmd FileType tex inoremap ,bf \textbf{}<Esc>T{i
autocmd FileType tex inoremap ,ct \citet{}<Esc>T{i
autocmd FileType tex inoremap ,cp \citep{}<Esc>T{i

"Markdown
autocmd FileType markdown set linebreak spell

""""""PLUGIN SETTINGS""""""

"indentLine
let g:indentLine_fileTypeExclude = ['markdown']

"nvim-colorizer
lua require'colorizer'.setup()

"fzf
let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'highlight': 'Todo', 'border': 'sharp' } }
let g:fzf_colors =
\ { 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

"lightline
let g:lightline = {
	\ 'colorscheme': 'one',
	\ 'active': {
	\	'left': [ [ 'mode', 'paste' ],
	\			  [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
	\  },
	\  'component_function': {
	\    'gitbranch': 'FugitiveHead'
	\  },
    \ 'separator': { 'left': '', 'right': '' },
    \ 'subseparator': { 'left': '', 'right': '' }
	\ }

"netrw
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
let g:NetrwIsOpen=0

function! ToggleNetrw()
    if g:NetrwIsOpen
        let i = bufnr("$")
        while (i >= 1)
            if (getbufvar(i, "&filetype") == "netrw")
                silent exe "bwipeout " . i 
            endif
            let i-=1
        endwhile
        let g:NetrwIsOpen=0
    else
        let g:NetrwIsOpen=1
        silent Lexplore
    endif
endfunction
