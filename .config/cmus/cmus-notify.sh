#!/bin/sh
#
# cmus-status-display
#
# Usage:
#   in cmus command ":set status_display_program=cmus-status-display"
#
# This scripts is executed by cmus when status changes:
#   cmus-status-display key1 val1 key2 val2 ...
#
# All keys contain only chars a-z. Values are UTF-8 strings.
#
# Keys: status file url artist album discnumber tracknumber title date
#   - status (stopped, playing, paused) is always given
#   - file or url is given only if track is 'loaded' in cmus
#   - other keys/values are given only if they are available
#  

LOG_FILE="/tmp/cmus-status.log"

while test $# -ge 2 ; do
	eval _$1='$2'
	shift
	shift
done

sanitize_path() {
    sed 's/\.$/_/g' \
    | sed 's@/@_@g' \
    | sed 's/:/_/g' \
    | sed 's/Various Artists/Compilations/g' \
    | sed 's/\?/_/g' \
    | sed 's/\*/_/g'
}

if test -n "$_file" ; then
	h=$(($_duration / 3600))
	m=$(($_duration % 3600))

	duration=""
	test $h -gt 0 && dur="$h:"
	duration="$dur$(printf '%02d:%02d' $(($m / 60)) $(($m % 60)))"

    COVER_PATH="$(cmus-remote -Q | grep file | cut -d" " -f 2- | rev | cut -d"/" -f 2- | rev)"/cover.jpg

    if [ -f "$COVER_PATH" ] ; then
            notify-send --icon="$COVER_PATH" "[$_status]" "$_title\n$duration\n$_artist - $_album ($_date)"
            feh --bg-center --save "$COVER_PATH"
    else
        notify-send "[$_status]" "$_title\n$duration\n$_artist - $_album ($_date)"
    fi
elif test -n "$_url" ; then
	notify-send "[$_status]" "$_url - $_title"
#else
#	notify-send "[$_status]"
fi
