#!/bin/bash

commFile=$1

/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME add $1

echo "Please describe the commit."

read commit

/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME commit -m "$commit"

/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME push
