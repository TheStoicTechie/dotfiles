#
# ~/.bashrc
#

# If not running interactively, don't do anything
export HISTCONTROL=ignorespace
[[ $- != *i* ]] && return
PS1='[\u@\h \W]\$ '
set -o vi
HISTSIZE=2000
HISTFILESIZE=2000

#FZF
[ -f /usr/share/fzf/key-bindings.bash ] && source /usr/share/fzf/key-bindings.bash
[ -f /usr/share/fzf/completion.bash ] && source /usr/share/fzf/completion.bash

case ${TERM} in
  st*|xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s %s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'

    ;;
esac

if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi
